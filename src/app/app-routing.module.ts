import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthendicationGuard } from './core/guards/authendication.guard';

const routes: Routes = [
  { path: '',  redirectTo: '/dashboard', pathMatch: 'full'},
  { path: 'login',loadChildren: () => import('./features/authendication/authendication.module').then(m => m.AuthendicationModule)},
  { path: 'dashboard', loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [AuthendicationGuard] },
  //{ path: '**', component: PageNotFoundComponent }, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
