import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { LoginService } from 'src/app/core/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmitted  =  false;
  errorMsg='';
  EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor( 
      private loginService : LoginService,
      private router: Router,
      private formBuilder: FormBuilder ) { }

  ngOnInit(): void {
      this.loginForm  =  this.formBuilder.group({
        email: ['', [Validators.required,  Validators.email]],
        password: ['', [Validators.required,  Validators.minLength(4)]]
    });
  }

  get formControls() { return this.loginForm.controls; }

  doLogin(){
    console.log(this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
    const userDetail = {
        email : this.loginForm.value.email,
        password : this.loginForm.value.password,
      }
      if( this.loginService.doUserLogin(userDetail)) {
        this.router.navigate(['/dashboard/add-payment']);
      } else {
        this.errorMsg = 'Invalid Login';
      }
  } 
}
