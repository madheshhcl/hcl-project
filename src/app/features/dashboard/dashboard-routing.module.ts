import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPaymentComponent } from './components/add-payment/add-payment/add-payment.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ListPostsComponent } from './components/list-posts/list-posts.component';




const routes: Routes = [
  { path: '', component:DashboardComponent ,
  children: [
      { path: 'add-payment', component:AddPaymentComponent },
      { path: 'list', component:ListPostsComponent }
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
