import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListPostsComponent } from './components/list-posts/list-posts.component';
import { AddPaymentComponent } from './components/add-payment/add-payment/add-payment.component';






@NgModule({
  declarations: [
    DashboardComponent,
    AddPaymentComponent
    ,
    ListPostsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
