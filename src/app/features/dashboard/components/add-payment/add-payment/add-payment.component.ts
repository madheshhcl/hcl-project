import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../../services/dashboard.service';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css']
})
export class AddPaymentComponent implements OnInit {

  constructor(private dashboardService : DashboardService, private formBuilder: FormBuilder,  private router: Router,) { }
  addPaymentForm: FormGroup;
  isSubmitted  =  false;
  transctions= [];
  errorMsg='';
  ngOnInit(): void {
    this.addPaymentForm  =  this.formBuilder.group({
      firstName: [''],
      LastName: [''],
      transactionDescription: ['', [Validators.required,  Validators.minLength(4)]],
      amount: ['', [Validators.required]],
      categoryType: ['', [Validators.required]],
      paymentType: ['', [Validators.required]],
  });
    this.dashboardService.getPosts().subscribe((response :any)=> {
        console.log(response);
        this.transctions = response;
    });
  }
  
  get formControls() { return this.addPaymentForm.controls; }

  addPayment(){
    console.log(this.addPaymentForm);
    this.isSubmitted = true;
    if(this.addPaymentForm.invalid){
      this.errorMsg = 'Please fill all fields';
      return;
    }
    let newTransaction = {
      customerId : 1,
      "transactionDescription": this.addPaymentForm.value.transactionDescription,
      "category": this.addPaymentForm.value.categoryType,
      "payment":{
          "mode":this.addPaymentForm.value.paymentType,
          "amount":this.addPaymentForm.value.amount
      },
     "initialBalance":1000,
     "remaingBalance":5000,
      "dateTime":new Date()
      };
    this.dashboardService.transctions.push(newTransaction);
    this.router.navigate(['/dashboard/list']);

  }
  
}
