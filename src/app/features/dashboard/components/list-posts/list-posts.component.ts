import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {
  transctions:any;
  constructor(private dashboardService : DashboardService,) { }

  ngOnInit(): void {
    this.transctions = this.dashboardService.transctions;
  }

}
