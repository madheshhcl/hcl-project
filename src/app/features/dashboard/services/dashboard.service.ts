import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrlHost = environment.api_url;
  transctions = [];
  constructor(private httpClient: HttpClient) { }

  getPosts(): Observable<any> {
    this.apiUrlHost = environment.api_url;
    const apiUrl = this.apiUrlHost +'transactions';
    return this.httpClient.get(apiUrl);
  }

  addPost(newTransacction){
    this.apiUrlHost = environment.api_url;
    const apiUrl = this.apiUrlHost +'transactions';
    return this.httpClient.post(apiUrl,newTransacction);
  }
}
