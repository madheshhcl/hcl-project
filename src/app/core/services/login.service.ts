import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  currentUser:any;
  constructor() { }

  doUserLogin(userData): boolean {
    if (userData.email === 'madhesh@gmail.com' && userData.password ==='test') {
      localStorage.setItem('user_details',userData);
      return true
    }
  }

  isUserLoggedIn():boolean {
    const isValidUser = localStorage.getItem('user_details');
    if(isValidUser) {
      return true;
    }
    return false;
  }
}
